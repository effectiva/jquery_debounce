/*global debounce */

$(function(){
  'use strict';


  $('input').on('keyup', debounce(function(){
    $('.output').append('<div>Triggered function!</div>');
  }, 500));


});

