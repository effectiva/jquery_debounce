# Debounce function

## How it works:
Returns a function, that, as long as it continues to be invoked, will not
be triggered. The function will be called after it stops being called for
N milliseconds. If `immediate` is passed, trigger the function on the
leading edge, instead of the trailing.


## Usage:

### Via Javascript
```javascript
  $('input').on('keyup', debounce(function(){
    $('.output').append('<div>Triggered function!</div>');
  }, 500));
```

| Argument   | type     | required | description                                                                                          |
| ---------- | -------  | -------- | ---------------------------------------------------------------------------------------------------- |
| func       | function |    yes   | this function which will be executed if debounce function isn't called after N miliseconds           |
| wait       | integer  |    yes   | number of miliseconds after which debounce callback function will be executed                        |
| immediate  | boolean  | optional | if `immediate` is passed, trigger the callback function on the leading edge, instead of the trailing |


### DEMO
For more information try provided demo.

In order to be able to try the demo you need to do the following:

  + ``git clone`` this repository
  + run ``bower install``
  + run ``http-server``

### Install instructions
__To install latest version run:__

``bower install https://bitbucket.org/effectiva/jquery_debounce#master``

__To install stable release version run:__

``bower install https://bitbucket.org/effectiva/jquery_debounce.git``